<head>
    <jsp:directive.include
            file="/WEB-INF/jsp/prelude/include-head-meta.jspf" />
    <title>Register</title>
</head>
<body>
<body>
<div class="container">
    <form class="form-signin" method="POST">

        <h1 class="h3 mb-3 font-weight-normal">注册</h1>

        <label for="inputEmail" class="sr-only">请输出电子邮件</label>
        <input type="email" id="inputEmail" name="email" class="form-control"
               placeholder="请输入电子邮件" required autofocus>

        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control"
            placeholder="请输入密码" required>

        <label for="inputName" class="sr-only">Password</label>
        <input type="text" id="inputName" name="name" class="form-control"
               placeholder="请输入姓名">

        <label for="inputPhoneNumber" class="sr-only">Password</label>
        <input maxlength="11" type="number" id="inputPhoneNumber" name="phoneNumber" class="form-control"
               placeholder="请输入手机号">

        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
    </form>
</div>
</body>
</body>