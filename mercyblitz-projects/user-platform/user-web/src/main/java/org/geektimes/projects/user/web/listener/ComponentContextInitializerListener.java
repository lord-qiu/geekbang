package org.geektimes.projects.user.web.listener;

import org.geektimes.projects.context.ComponentContext;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * {@link ComponentContext} 初始化器
 * 与 ContextLoaderListener 类似
 */
//@WebListener
public class ComponentContextInitializerListener implements ServletContextListener {

    private ServletContext servletContext;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        this.servletContext = sce.getServletContext();
        ComponentContext context = new ComponentContext();
        context.init(servletContext);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ComponentContext context = ComponentContext.getInstance();
        context.destory();
    }

//    protected Connection getConnection() {
//        Context context = null;
//        Connection connection = null;
//        try {
//            context = new InitialContext();
//            // All configured entries and resources are placed in the java:comp/env portion of the JNDI namespace
//            Context envContext = (Context) context.lookup("java:comp/env");
//            // 依赖查找
//            DataSource dataSource = (DataSource) envContext.lookup("jdbc/UserPlatformDB");
//            connection = dataSource.getConnection();
//        } catch (NamingException | SQLException e) {
//            // e.printStackTrace();
//            servletContext.log(e.getMessage(), e);
//        }
//        if (connection != null) {
//            servletContext.log("获取 JNDI 数据库连接成功！");
//        }
//        return connection;
//    }
}
